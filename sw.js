const CACHE_NAME = `virtual-walk-v1`;

// Use the install event to pre-cache all initial resources.
self.addEventListener('install', event => {
    event.waitUntil((async () => {
        const cache = await caches.open(CACHE_NAME);
        cache.addAll([
            '/',
            '/index.html',
            '/global.css',
            '/index.css',
            '/assets/app_icon/virtual_walk_icon.png',
            '/assets/index-background.jpg',
            '/assets/index-emblem.png'
        ]);
    })());
});

self.addEventListener('fetch', event => {
    event.respondWith((async () => {
        const cache = await caches.open(CACHE_NAME);

        // Get the resource from the cache.
        const cachedResponse = await cache.match(event.request);
        if (cachedResponse) {
            return cachedResponse;
        } else {
            try {
                // If the resource was not in the cache, try the network.
                const fetchResponse = await fetch(event.request);

                // Save the resource in the cache and return it.
                cache.put(event.request, fetchResponse.clone());
                return fetchResponse;
            } catch (e) {
                // The network failed.
            }
        }
    })());
});

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(function (registration) {
        console.log('Service Worker Registered with scope:', registration.scope);
    }).catch(function (error) {
        console.log('Service Worker registration failed:', error);
    });
};

/*
该文件将充当 PWA 的服务辅助角色。上面的代码侦听该事件，并使用它来缓存应用运行所需的所有资源：起始 HTML 页、转换器 JavaScript 文件和转换器 CSS 文件。sw.jsinstall

该代码还会拦截每次应用向服务器发送请求时发生的事件，并应用缓存优先策略。Service Worker 返回缓存的资源，以便应用可以脱机工作，如果失败，请尝试从服务器下载。fetch
*/