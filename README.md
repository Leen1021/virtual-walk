# nginx 相关文件备份
在目录./nginx_files下
# 目录结构
网站根目录(在服务器文件系统中的路径是/usr/share/nginx/virtual-walk-html/)存放index页面的html, js, css, 以及assets目录和全局的配置

assets目录存放所有的资源文件, 这样无论在哪里, 调用本地资源都使用绝对路径, 如`src="/assets/xxx.png"`

出了index页之外的其他页面都是根目录下的一个文件夹, 文件夹名就叫`<页面的名字>`, 而页面文件夹下放置本页面的html, js, css文件, 如:
```
/usr/share/nginx/virtual-walk-html/
|
`-- vr-page
    |-- vr-page-css.css
    |-- vr-page.html
    `-- vr-page.js
```

index页面的三个文件:index.html, index.js, index.css全部放在根目录

页面的html导入该页面的js,css, 使用相对路径, 而导入全局资源和文件使用绝对路径

# 命名风格
- 对于assets下的资源:`<页面名字>-<自己定的资源名字><文件后缀>`
    - 如index页面中的背景图片是`index-background.png`

- 对于html元素的id或者class:`<元素的功能角色>-<html标签>`
    - 如index页面中用于容纳背景图片的div就是`id="index-background-div"`

- 对于css变量名:`<页面名字>-<自己定的变量名>`
    - 如index页面中的大标题的字号是`--index-bigtitle-fontsize: calc((var(--min-window-side-len)) * 0.05); `

# 引用风格
- 对于引用同一页面文件夹下的资源, 应使用相对路径引用
    - 如在`select-view.html`中引用它的css, 应使用`<link rel="stylesheet" href="select-view.css">`
- 对于引用其他文件夹下的资源, 应使用绝对路径引用
    - 如在`vr-page.html`中引用被指定为全局生效的样式表`global.css`, 应使用`<link rel="stylesheet" href="/global.css">`
    - 如在`select-view.html`中引用assets中的资源, 应使用`<img src="/assets/select-view-pic.png" alt="Button" class="animated-button" id="select-view-pic">`
    
# css
css的所有值尽量都定义在/global.css下的:root中, 这使得样式可以更方便地管理