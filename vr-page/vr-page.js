
// Wait for the DOM to be loaded
document.addEventListener('DOMContentLoaded', function () {
    // Check if HLS is supported
    if (Hls.isSupported()) {
        // Get the video element
        var video = document.getElementById('videoPlayer');
        // Create a new instance of Hls
        var hls = new Hls();
        // Load the HLS source
        hls.loadSource('https://virtualwalk.oss-cn-shanghai.aliyuncs.com/test-channel/playlist.m3u8');
        // Attach the media to the video element
        hls.attachMedia(video);
        // When the manifest is parsed, start playing the video
        hls.on(Hls.Events.MANIFEST_PARSED, function () {
            video.play();
        });
    }
});