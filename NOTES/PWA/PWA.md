渐进式Web应用（Progressive Web App，PWA）是一个使用web平台技术构建的[应用程序](https://baike.baidu.com/item/应用程序/5985445?fromModule=lemma_inlink)，但它提供的用户体验就像一个特定平台的应用程序。 [2]在移动端利用标准化框架，让网页应用呈现和原生应用相似的体验。 [1]





## 目录

1. 1[原理](https://baike.baidu.com/item/PWA/22378897?fr=ge_ala#1)
2. 2[应用](https://baike.baidu.com/item/PWA/22378897?fr=ge_ala#2)
3. 3[特点](https://baike.baidu.com/item/PWA/22378897?fr=ge_ala#3)

## 原理

渐进式Web应用程序 (PWA) 使用现代API进行构建和增强，提供增强的功能、可靠性和可安装性，同时只需一个代码库就可以借助任何设备触及任何人、任何地方。 [3]

## 应用

Twitter和Flipboard都推出了PWA，可以将它放在[Android](https://baike.baidu.com/item/Android/60243?fromModule=lemma_inlink)主屏。微软通过浏览器将PWA带到了Windows 10和Windows 11。 [1]

## 特点

PWA不能包含原生OS相关代码。PWA仍然是网站，只是在缓存、通知、后台功能等方面表现更好。Electron程序相当于包裹OS原生启动器（Launcher）的网站，未来，许多[Electron](https://baike.baidu.com/item/Electron/60878453?fromModule=lemma_inlink)程序可能转化为PWA。 [1]