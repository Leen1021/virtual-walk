# SSL私钥

```shell
[root@VM-12-16-centos https]# openssl genrsa -out nginx.key 2048
Generating RSA private key, 2048 bit long modulus
...........+++
...........................+++
e is 65537 (0x10001)
[root@VM-12-16-centos https]# ll
total 4
-rw-r--r-- 1 root root 1675 Apr 21 01:42 nginx.key

[root@VM-12-16-centos https]# openssl req -new -key nginx.key -out nginx.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:CN
State or Province Name (full name) []:Shanghai
Locality Name (eg, city) [Default City]:Shanghai
Organization Name (eg, company) [Default Company Ltd]:SBS
Organizational Unit Name (eg, section) []:Virtual Walk
Common Name (eg, your name or your server's hostname) []:Qinxiao
Email Address []:18721826298@163.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:virtualwalk20240421
An optional company name []:SBS

```

```shell
[root@VM-12-16-centos https]# openssl x509 -req -in nginx.csr -signkey nginx.key -out nginx.pem
Signature ok
subject=/C=CN/ST=Shanghai/L=Shanghai/O=SBS/OU=Virtual Walk/CN=Qinxiao/emailAddress=18721826298@163.com
Getting Private key

```

